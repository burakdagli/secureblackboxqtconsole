#include <QCoreApplication>
#include <QLibrary>
#include <QDebug>

#include <cstring>
#include <iostream>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "sbcustomcertstorage.h"
#include "sbmisc.h"
#include "sbpdf.h"
#include "sbpdfsecurity.h"
#include "sbutils.h"
#include "sbx509.h"
#include "sball.h"
#include "sbwincertstorage.h"
#include <QImage>

using namespace SecureBlackbox;

TSBPDFSignatureType paramSignatureType = stDocument;
bool paramInvisibleSignature = false;
std::string paramAuthor;
std::string paramReason;
std::string paramInputFile;
std::string paramOutputFile;


bool CopyFile(std::string fromFilename, std::string toFilename)
{
    std::ifstream src;
    std::ofstream dst;
    src.open(fromFilename.c_str(), std::ios::binary | std::ios::in);
    if (!src.is_open())
    {
        std::cout << "Unable to open file: " << fromFilename << std::endl;
        return false;
    }

    dst.open(toFilename.c_str(), std::ios::binary | std::ios::out);
    if (!dst.is_open())
    {
        std::cout << "Unable to open file: " << toFilename << std::endl;
        return false;
    }

    dst << src.rdbuf();

    dst.close();
    src.close();
    return true;
}

void GetTempFilename(std::string &filename)
{
    filename = "tmp.pdf";
    //filename.resize(L_tmpnam);
    //tmpnam(&filename.front());
}

void SignDocument()
{
    std::string tmpFilename;
    tmpFilename = paramInputFile;
    GetTempFilename(tmpFilename);
    if (!CopyFile(paramInputFile, tmpFilename))
    {
        std::cout << "Signing failed " << std::endl;
        return;
    }

    uint8_t Success = 0;

    // using native stream
    std::fstream fs;
    fs.open(tmpFilename.c_str(), std::ios::binary | std::ios::in | std::ios::out);
    if (fs.is_open())
    {
        TElCallbackStream cs(fs);

        TElPDFDocument Document(NULL);
        try
        {
            Document.Open(cs);
            if (Document.get_Encrypted())
            {
                std::cout << "The document is encrypted and cannot be signed" << std::endl;
                Document.Close(false);
                fs.close();
                return;
            }

            TElPDFPublicKeySecurityHandler PublicKeyHandler(NULL);
            int index = Document.AddSignature();
            TElPDFSignature *Sig = Document.get_Signatures(index);
            Sig->set_Handler(PublicKeyHandler);
            Sig->set_AuthorName(paramAuthor);
            Sig->set_Reason(paramReason);

            time_t timer;
            time(&timer);
            Sig->set_SigningTime(timer);

            Sig->set_Invisible(paramInvisibleSignature);
            Sig->set_SignatureType(paramSignatureType);


           /*std::vector<uint8_t> * vector_ = new std::vector<uint8_t>;

            for (unsigned i=0; i<tmp.width(); ++i)
            {
               for (unsigned j=0; j<tmp.height(); ++j)
                   vector_->push_back((uint8_t)tmp.pixel(i,j));
            }

            TElPDFSignatureWidgetProps *widgetProps = Sig->get_WidgetProps();
            widgetProps->set_BackgroundStyle(pbsCustom);
            widgetProps->get_Background()->set_Height(tmp.height());
            widgetProps->get_Background()->set_Width(tmp.width());
            widgetProps->get_Background()->set_Data(*vector_);
            widgetProps->get_Background()->set_ImageType(pitCustom);
            widgetProps->get_Background()->set_ColorSpaceType(pcstRGB);
            widgetProps->set_AutoStretchBackground(true);*/

            std::ifstream img;
            img.open("esign.jpg", std::ios::binary | std::ios::in);
            if (!img.is_open())
            {
               std::cout << "Unable to open file" << std::endl;
               return;
            }

            std::vector<uint8_t> buf;

            std::noskipws(img);
            img.seekg(0, img.end);
            buf.resize((int)img.tellg());
            img.seekg(0);
            img.read((char *)&buf.front(), buf.size());

            TElPDFSignatureWidgetProps *widgetProps = Sig->get_WidgetProps();
            widgetProps->set_AutoSize(false);
            widgetProps->set_Height(105);
            widgetProps->set_Width(75);
            widgetProps->set_BackgroundStyle(pbsCustom);
            widgetProps->get_Background()->set_Height(350);
            widgetProps->get_Background()->set_Width(250);
            widgetProps->get_Background()->set_Data(buf);
            widgetProps->get_Background()->set_ImageType(pitJPEG);
            widgetProps->set_HideDefaultText(true);
            widgetProps->set_AutoStretchBackground(true);
            widgetProps->get_CustomText()->Add("Ad-Soyad : Burak Dagli",5,8,5);
            widgetProps->get_CustomText()->Add("Sicil No : 040060232",5,16,5);

            std::string paramStorage = "MY";

            TElWinCertStorage WinCertStorage(NULL);
            WinCertStorage.get_SystemStores()->set_Text(paramStorage);

            TElX509Certificate *Cert = WinCertStorage.get_Certificates(0);
            TElMemoryCertStorage CertStorage(NULL);
            CertStorage.Clear();
            CertStorage.Add(Cert, true);

            PublicKeyHandler.set_CertStorage(CertStorage);
            PublicKeyHandler.set_SignatureType(pstPKCS7SHA1);
            PublicKeyHandler.set_CustomName("Adobe.PPKMS");

            Document.Close(true);
            Success = 1;
        }
        catch (SBException E)
        {
            std::cout << E.what() << std::endl;
            std::cout << "Stack trace: " << E.getErrorStackTrace().c_str() << std::endl;
        }

        fs.close();
    }
    else
        std::cout << "Unable to open file: " << tmpFilename << std::endl;

    if (Success)
    {
        CopyFile(tmpFilename, paramOutputFile);
        std::cout << "Signing process successfully finished" << std::endl;
    }
    else
        std::cout << "Signing failed" << std::endl;

    if (remove(tmpFilename.c_str()) != 0)
        std::cout << "Error deleting file" << std::endl;
}


int main() {

    try
    {
        SetLicenseKey("B23518DC0D9D6F583B498AC3718754742D5ABEC716F83E1AB957B13C5C88A7A093F9563B21C6F00ECA5F0E4F40ACFAAEB203906ABD96DA46B94A0833A38E7BF428B6C50CEDE39B8898CF6C6C65331E7A9B45ABAEEB0E857E8D7A9A1C1282BB03AE0F9DD77898594E9CFA2C5EC03C5275A37D965252F39026D51ADA667A05F87B4879F9372CA69F43C6F8053A75270814333961ABBB7A280A1D5D592D66F534B7B98B81ABBB51EB68B60E461E3E75D88E9BBC592DF899F075AD00990EE3CE104A7A47307225C8EBD8F7EFB408F49BC55078CC41F68709150EFD337C401568F7465FE5872EC765755E8ED03C429DE24FAB6D213195A3AC0E51962D8A882F770A56");
    }
    catch (SBException E)
    {
        std::cout << "Failed to set license key:" << std::endl;
        std::cout << E.what() << std::endl;
        return 0;
    }

    paramInputFile = "test.pdf";
    paramOutputFile = "result.pdf";
    paramInvisibleSignature = false;
    paramAuthor = "Burak Dağlı";
    paramReason = "Onay";

    SignDocument();
    return 0;
}

