#-------------------------------------------------
#
# Project created by QtCreator 2015-03-03T11:08:00
#
#-------------------------------------------------

QT       += core

QT       += gui

TARGET = SignConsole
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

Debug{
    message(Debug bulid)
    win32: LIBS += -LE:/SecureBlackBoxCPP/WrapperSources/ -llibsbbimp

    win32:!win32-g++: PRE_TARGETDEPS += E:/SecureBlackBoxCPP/WrapperSources/libsbbimp.lib
    else:win32-g++: PRE_TARGETDEPS += E:/SecureBlackBoxCPP/WrapperSources/liblibsbbimp.a
    QMAKE_CXXFLAGS += /D_ITERATOR_DEBUG_LEVEL=0
}

Release{
    message(Release bulid)
    win32: LIBS += -LC:/Users/BDagli/Downloads/secbboxcpp_win/WrapperSources/ -llibsbbimp

    win32:!win32-g++: PRE_TARGETDEPS += C:/Users/BDagli/Downloads/secbboxcpp_win/WrapperSources/libsbbimp.lib
    else:win32-g++: PRE_TARGETDEPS += C:/Users/BDagli/Downloads/secbboxcpp_win/WrapperSources/liblibsbbimp.a
}


INCLUDEPATH += E:/SecureBlackBoxCPP/WrapperSources/include
DEPENDPATH += E:/SecureBlackBoxCPP/WrapperSources/include
INCLUDEPATH += E:/SecureBlackBoxCPP/WrapperSources/include/all
DEPENDPATH += E:/SecureBlackBoxCPP/WrapperSources/include/all


